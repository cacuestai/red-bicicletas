const nodemailer = require("nodemailer");
const sgTransform = require('nodemailer-sendgrid-transport');

let mailConfig;
if (process.env.NODE_ENV == 'production') {
  const opinions = {
    auth: {
      api_key: process.env.SENDGRID_API_SECRET
    }
  }
  mailConfig = sgTransform(opinions);

} else {
  if (process.env.NODE_ENV == 'staging') {
    console.log('xxxxxxxxxx');
    const opinions = {
      auth: {
        api_key: process.env.SENDGRID_API_SECRET
      }
    }
    mailConfig = sgTransform(opinions);

  } else {
    mailConfig = {
      host: "smtp.ethereal.email",
      port: 587,
      auth: {
        user: process.env.ethereal_user,
        pass: process.env.ethereal_pwd
      }
    };
  }
}

module.exports = nodemailer.createTransport(mailConfig);
