var mongoose = require('mongoose');

var Bicicleta = require('../../models/bicicleta');

describe('Testing bicicletas', function () {
    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/red_bicicletas'
        mongoose.connect(mongoDB, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        const db = mongoose.connection
        db.on('error', console.error.bind(console, 'connection:error'))
        db.once('open', function () {
            console.log('We are connected to test database')
            done();
        });
    });

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err)
            done()
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crear una bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [4.215151545, -74.1664153165])

            expect(bici.code).toBe(1)
            expect(bici.color).toBe("verde")
            expect(bici.modelo).toBe("urbana")
            expect(bici.ubicacion[0]).toEqual(4.215151545)
            expect(bici.ubicacion[1]).toEqual(-74.1664153165)
        });
    });

    describe('Bicicleta.allbicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allbicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
                done()
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('agregar una bici', (done) => {
            var aBici = new Bicicleta({
                code: 1,
                color: "azul",
                modelo: "montaña"
            });
            Bicicleta.add(aBici, function (err, newBici) {
                if (err) console.log(err)
                Bicicleta.allbicis(function (err, bicis) {
                    expect(bicis.length).toEqual(1)
                    expect(bicis[0].code).toEqual(aBici.code)
                    done()
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('debe devolver el elemento por código', (done) => {
            Bicicleta.allbicis(function (err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({
                    code: 1,
                    color: "azul",
                    modelo: "urbana"
                });
                Bicicleta.add(aBici, function (err, newBici) {
                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({
                        code: 2,
                        color: "blanca",
                        modelo: "montaña"
                    });
                    Bicicleta.add(aBici2, function (err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function (error, targetBici) {
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        });
                    });
                });
            });
        });
    });
});


// /*
//  * Para evitar la repetición de código utilizamos el método beforeEach
//  * Antes de cada test se ejecuta la instrucción, la colleción de bicicletas inicia vacía
// */
// beforeEach(() => { Bicicleta.allBicis = []; });

// // Qrupo de test que va a ser foco en el método allbicis (models/bicicleta.js) 
// describe('Bicicleta.allBicis', () => { //() => { le pasamos una función lamba
//     // Que es lo que quiero probar
//     it('comienza vacia', () => {
//         // Test 
//         expect(Bicicleta.allBicis.length).toBe(0);
//     });

// });

// describe('Bicicleta.add', () => {
//     it('agregamos una', () => {
//         /*
//          * Testeamos nuevamente que bicicletas es cero.
//          * Como es el estado previo
//         */
//         expect(Bicicleta.allBicis.length).toBe(0);

//         // Creamos una bicicleta y la agregamos 
//         var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424,-58.3861497]);
//         Bicicleta.add(a);

//         // Como es el estado posterior
//         expect(Bicicleta.allBicis.length).toBe(1);

//         // Pedimos que la primer posición sea la bicicleta que acabamos de agregar
//         expect(Bicicleta.allBicis[0]).toBe(a);

//     });
// });

// describe('Bicicleta.findById', () => {

//     // Supongamos que vamos a buscar por el id 1
//     it('debe devolver la bici con id 1', () => {

//         // Aseguramos que la lista comience vacía
//         expect(Bicicleta.allBicis.length).toBe(0);

//         // Creamos dos bicicletas y las agregamos
//         var aBici = new Bicicleta(1, "verde", "urbana");
//         var aBici2 = new Bicicleta(2, "rojo", "montaña");
//         Bicicleta.add(aBici);
//         Bicicleta.add(aBici2);

//         var targetBici = Bicicleta.findById(1);
//         expect(targetBici.id).toBe(1);
//         expect(targetBici.color).toBe(aBici.color);
//         expect(targetBici.modelo).toBe(aBici.modelo);
//     });
// });