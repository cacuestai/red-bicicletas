var mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
var Reserva = require('./reserva');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
// El salt lo que le va a hacer es introducirle cierta aleatoriedad a esa encriptación
const saltRounds = 10;

const Token = require('../models/token');
const mailer = require('../mailer/mailer');

var Schema = mongoose.Schema;


//const { defaultMaxListeners } = require('nodemailer/lib/mailer');


const validateEmail = function(email) {
    // Objeto Regex, regular expressions
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    // El metodo test valida que se cumpla con la expresion regular, sino no es un email valido
    return re.test(email);
}

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        // Sí cuando se cree el nombre hay espacios vacíos al principio y al final, se van a sacar
        trim: true,
        // Requerir el nombre
        required: [true, 'El nombre es obligatorio']
    },

    email: { 
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        // Al momento de guardar pone todo en minúscula
        lowercase: true,
        /**
         * Lo emails deben ser únicos, el email será usado como clave del usuario
         * El valor unique en Mongoose no está definido. Entonces se instala la librería
         */
        //unique: true,
        /**
         * Le pasamos la función validateEmail, se ejecuta sobre el campo email al momento de guardar
         * En el caso de que no sea válido, le ponemos un mensaje de error
         */
        validate: [validateEmail, 'Por favor ingrese un email valido'],
        /**
         * Todo lo que se este ingresando debe pasar por el match, criterio de validación a nivel dato
         * Diferencia entre validate y match
         * validate se ejecuta al momento del guardado, forma parte de mongoose
         * El match corre a nivel Mongo 
         * Con los dos es suficiente para cubrirnos de cualquier tipo de diferencia o error
         */
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/] 
    },

    password: { 
        type: String,
        required: [true, 'El password es obligatorio']
    },


    /**
     * Valores para el token
     * El Token es una clave larga, en el momento en que una persona se registre en nuestro sitio, 
     * le vamos a enviar un email, ese email va tener un token que unifica, 
     * va a estar asociado a una cuenta
     */
    passwordResetToken: String,
    passwordResetTokenExpires: Date,

    verificado: {
        type: Boolean,
        /**
         * Utilizando mongoose, el valor por defecto es false y así aparecerá en la bd de Mongo
         * Cuando el usuario ingrese a su email, va a poder verificar la cuenta y este valor se va a pasar a true.
         */
        default: false
    }

});

/**
 * El PATH referencia al atributo email.
 * Los plugin son módulos o librerías que no son parte directa de mongoose
 * se incorporan para hacer cosas que las librerías no permiten, como validación del unique
 * entre otras validaciones
 */
usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe con otro usuario'});

/**
 * Antes de un pre-save se ejecuta la función
 */
usuarioSchema.pre('save', function (next) {

    // si el password cambio, encriptar el password
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

// Comparación de los passwords
// el password va a ser un string plano, lo encriptamos y lo comparamos con la encriptación del mismo password.
usuarioSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
}


usuarioSchema.methods.reservar = function (biciId, desde, hasta, cb) {
    var reserva = new Reserva({
        usuario: this._id,
        bicicleta: biciId,
        desde: desde,
        hasta: hasta
    });
    console.log(reserva);
    reserva.save(cb);
}

usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function (err) {
        if (err) { return console.log(err.message); }

        const mailOptions = {
            from: 'No-reply@bicicletas.com',
            to: email_destination,
            subject: 'Verificación de cuenta',
            text: 'Hola,\n\n' + 'Por favor, para verificar su cuenta haga clic en este link: \n' + 'http://localhost:5000' + '\/token/confirmation\/' + token.token + '\n'
        };

        mailer.sendMail(mailOptions, function (err) {
            if (err) { return console.log(err.message); }

            console.log('Se ha enviado un email de bienvenida a: ' + email_destination + '.');
        });
    });
}

// Restablecer el password
usuarioSchema.methods.resetPassword = function (cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function (err) {
        if (err) { return cb(err); }

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Reseteo de password de cuenta ',
            text: 'Hola,\n\n' + 'Por favor, para resetear el password de su cuenta haga click en este link: \n' + 'http://localhost:5000' + '\/resetPassword\/' + token.token + '.\n'
        };

        mailer.sendMail(mailOptions, function (err) {
            if (err) { return cb(err); }

            console.log('Se envio un email para resetear el password a: ' + email_destination + '.');
        });

        cb(null);
    });
}

module.exports = mongoose.model('Usuario', usuarioSchema); 